package com.mxt.core.cryptic;

/**
 * 自加解密接口
 */
public interface ADESInterface {
    public <T> T encryptSelf();
    
    public <T> T decryptSelf();
}
