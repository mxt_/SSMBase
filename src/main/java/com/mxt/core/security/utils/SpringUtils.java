package com.mxt.core.security.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;

public final class SpringUtils implements ApplicationContextAware {
    
    private static ApplicationContext context;
    
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
    
    public static ApplicationContext getContext() {
        return context;
    }
    
    public static Object getBean(String beanName) {
        if (null != context) {
            boolean b = context.containsBean(beanName);
            if (b) {
                return context.getBean(beanName);
            }
        }
        return null;
    }
    
    public static <T> Map<String, T> getBeansOfType(Class<T> calzz) {
        if (null != context) {
            return context.getBeansOfType(calzz);
        } else {
            return null;
        }
    }
    
    public static boolean containsBean(String beanName) {
        return context.containsBean(beanName);
    }
    
    
}
