package com.mxt.core.security.constant;


public class ConstantKey {

    /**
          * 签名key
     */
    public static final String SIGNING_KEY = "spring-security-@Jwt!&2021^#";
    
    public static final String SIGNING_WALLET = "WALLET";
}
