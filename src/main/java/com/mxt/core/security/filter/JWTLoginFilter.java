package com.mxt.core.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mxt.core.security.bean.AuthBean;
import com.mxt.core.security.constant.ConstantKey;
import com.mxt.core.util.view.InvokeResult;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * 验证用户名密码正确后，生成一个token，并将token返回给客户端
 * 该类继承自UsernamePasswordAuthenticationFilter，重写了其中的2个方法
 * attemptAuthentication ：接收并解析用户凭证。
 * successfulAuthentication ：用户成功登录后，这个方法会被调用，我们在这个方法里生成token。
 * <p>
 * js ajax请求调用
 * beforeSend : function(request) {
 * request.setRequestHeader("Authorization", sessionStorage.getItem("Authorization"));
 * }
 *
 * @author
 */
public class JWTLoginFilter extends UsernamePasswordAuthenticationFilter {
    private static final Logger logger = LoggerFactory.getLogger(JWTAuthenticationFilter.class);
    private AuthenticationManager authenticationManager;
    
    
    public JWTLoginFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }
    
    private int expireSecond = 30 * 60;//秒
    
    // 接收并解析用户凭证
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException {
        try {
            AuthBean auth = new ObjectMapper().readValue(req.getInputStream(), AuthBean.class);
            String regNo = auth.getRegNo();
            String openId = auth.getOpenId();
            
            
            if (openId == null || "".equals(openId)) {
                logger.info("未找到识别码");
                res.setStatus(HttpStatus.UNAUTHORIZED.value());
                res.setCharacterEncoding("UTF-8");
                res.getWriter().print(InvokeResult.failure("未找到识别码").toString());
                res.getWriter().close();
                return null;
            }
            
            //TODO 设置权限校验规则
            List<String> aanlysisList = new ArrayList<>();
            
            if (aanlysisList.size() > 0) {
                String aanlysis = aanlysisList.get(0);
                return authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                aanlysis,
                                "123456",
                                new ArrayList<>())
                );
            } else {
                logger.info("用户不存在");
                res.setStatus(HttpStatus.UNAUTHORIZED.value());
                res.setCharacterEncoding("UTF-8");
                res.getWriter().print(InvokeResult.failure("用户不存在").toString());
                res.getWriter().close();
                return null;
            }
            
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    // 用户成功登录后，这个方法会被调用，我们在这个方法里生成token
    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {
        // builder the token
        String token = null;
        try {
            Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();
            // 定义存放角色集合的对象
            List roleList = new ArrayList<>();
            for (GrantedAuthority grantedAuthority : authorities) {
                roleList.add(grantedAuthority.getAuthority());
            }
            // 设置过期时间
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            //calendar.add(Calendar.DAY_OF_MONTH, 30);//30天
            calendar.add(Calendar.SECOND, expireSecond);
            Date time = calendar.getTime();
            token = Jwts.builder()
                    .setSubject(auth.getName() + "-" + roleList)
                    .setExpiration(time) // 设置过期时间
                    .signWith(SignatureAlgorithm.HS512, ConstantKey.SIGNING_KEY) //采用什么算法是可以自己选择的，不一定非要采用HS512
                    .compact();
            // 登录成功后，返回token到header里面
            response.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);
            response.setCharacterEncoding("UTF-8");
            response.getWriter().print(InvokeResult.success().toString());
            response.getWriter().close();
            //redis存储
//            JedisUtil.save(auth.getName(), token, expireSecond);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
