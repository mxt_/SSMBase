package com.mxt.controller.sys;

import com.alibaba.fastjson.JSON;
import com.mxt.bean.SysRes;
import com.mxt.core.util.view.InvokeResult;
import com.mxt.service.SysResService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;


/**
 * @author mengxt
 */
@Controller
@RequestMapping("/sys/res")
public class SysResController {
    @Resource
    private SysResService sysResService;
    
    
    @RequestMapping("")
    public String index() {
        return "/sys/res_index";
    }
    
    @RequestMapping("/getTreeGridView")
    @ResponseBody
    public String getTreeGridView() {
        return sysResService.getTreeGridView();
    }
    
    @RequestMapping("/add")
    public String add(Integer id, Model model) {
        if (id != null) {
            SysRes sysRes = sysResService.selectByPrimaryKey(id);
            if (sysRes != null) {
                Integer pid = sysRes.getPid();
                if (pid != null) {//获取到的父级id不为空则获取父级权限
                    SysRes pRes = sysResService.selectByPrimaryKey(pid);
                    model.addAttribute("pRes", pRes);
                }
            }
            model.addAttribute("sysRes", sysRes);
        }
        
        model.addAttribute("jsonTree", JSON.toJSONString(sysResService.getZtreeViewList()));
        return "/sys/res_add";
    }
    
    @RequestMapping("/saveRes")
    @ResponseBody
    public String saveRes(SysRes sysRes) {
        int result = 0;
        if (sysRes != null) {
            if (sysRes.getId() != null) {
                result = sysResService.updateByPrimaryKeySelective(sysRes) > 0 ? 2 : -1;
            } else {
                result = sysResService.insert(sysRes) > 0 ? 1 : -1;
            }
        }
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("code", "success");
        if (result == 1) {
            mapData.put("result", "保存成功");
        } else if (result == 2) {
            mapData.put("result", "修改成功");
        } else {
            mapData.put("code", "failure");
            mapData.put("result", "权限保存失败，请检查！");
        }
        return JSON.toJSONString(mapData);
    }
    
    @RequestMapping("/setEnabled")
    @ResponseBody
    public String setEnabled(Integer resId, Integer status) {
        sysResService.setEnabled(resId, status);
        return JSON.toJSONString(InvokeResult.success("设置成功"));
    }
    
    
    @RequestMapping("/delete")
    @ResponseBody
    public String delete(Integer id) {
        sysResService.deleteRes(id);
        sysResService.deleteByResId(id);
        return JSON.toJSONString(InvokeResult.success("删除成功"));
    }
}
