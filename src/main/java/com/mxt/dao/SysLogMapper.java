package com.mxt.dao;

import com.mxt.bean.SysLog;

public interface SysLogMapper extends BaseMapper<SysLog> {
    Integer removeAllLog();
}