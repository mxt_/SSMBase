package com.mxt.dao;

import com.mxt.bean.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysRoleMapper extends BaseMapper<SysRole> {
    List<SysRole> getSysRoleIdList(Integer uid);
    
    Integer setVisible(@Param("status") Integer status, @Param("ids") String ids);
    
    List<SysRole> getSysRoleNamelist();
    
    List<SysRole> getSysRoles(Integer uid);
    
}