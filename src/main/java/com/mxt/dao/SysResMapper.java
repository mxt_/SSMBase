package com.mxt.dao;

import com.mxt.bean.SysRes;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysResMapper extends BaseMapper<SysRes> {
    List<SysRes> findResByRoleId(String ids, Integer type);
    
    List<SysRes> findResByAdmin(@Param("type") Integer type);
    
    SysRes getByUrl(String url);
    
    List<SysRes> getSysMenus(Integer roleId);
}