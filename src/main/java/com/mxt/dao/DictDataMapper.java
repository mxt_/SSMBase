package com.mxt.dao;

import com.mxt.bean.DictData;

public interface DictDataMapper extends BaseMapper<DictData> {
    int deleteByTypeId(Integer id);
}