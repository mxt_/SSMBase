package com.mxt.dao;

import com.mxt.bean.DictType;

public interface DictTypeMapper extends BaseMapper<DictType> {
}