package com.mxt.dao;

import com.mxt.bean.SysUserRole;

import java.util.List;

public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
    Integer deleteByUserId(Integer id);
    
    Integer deleteByRoleId(Integer id);
    
    void insertByBatch(List<SysUserRole> sysUserRoleList);
}