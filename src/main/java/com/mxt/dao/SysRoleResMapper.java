package com.mxt.dao;

import com.mxt.bean.SysRoleRes;

import java.util.List;

public interface SysRoleResMapper extends BaseMapper<SysRoleRes> {
    Integer deleteByResId(Integer id);
    
    Integer deleteByRoleId(Integer id);
    
    void insertByBatch(List<SysRoleRes> sysRoleResList);
}