package com.mxt.service;

import com.mxt.bean.SysLog;

public interface SysLogService extends BaseService<SysLog> {
    void deleteAll();
}
