package com.mxt.service;

import com.mxt.bean.SysUser;
import com.mxt.core.util.view.InvokeResult;

import javax.servlet.http.HttpSession;
import java.util.Set;

public interface SysUserService extends BaseService<SysUser> {
    InvokeResult doLogin(HttpSession httpSession, String name, String pwd);
    
    InvokeResult savePwdUpdate(Integer id, String newPwd);
    
    Set<String> getPermissionSets(Integer id);
    
    InvokeResult setVisible(String ids, Integer visible);
    
    InvokeResult getRoleZtreeViewList(Integer uid);
    
    InvokeResult changeUserRoles(Integer id, String roleIds);
}
