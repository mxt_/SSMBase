package com.mxt.service;

import com.mxt.bean.SysRole;
import com.mxt.core.util.view.InvokeResult;

import java.util.List;

public interface SysRoleService extends BaseService<SysRole> {
    InvokeResult setVisible(String bids, Integer visible);
    
    InvokeResult getZtreeViewList(Integer type, Integer roleId);
    
    InvokeResult saveMenuAssign(String menuIds, Integer roleId);
    
    List<SysRole> getSysRoleNameList();
}
