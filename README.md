# SSMBase

Spring+SpringMVC+Mybatis Base Development Framework
- - -

- 更新日期
    + 2021-12-11
        + 更新文件目录为个人，增加其他工具类，修改项目Title
    + 2021-12-16
        + 精简部分冗余代码，添加注释，JQGridModel增加泛型
    + 2021-12-17
        + 增加Spring-Security支持，增加redisUtils工具类，增加Mybatis自动加密工具类
    + 2021-12-23
      + 修复Spring-Security无法访问iframe问题，修复部分bug